/**
 * Created by 4ant0m on 12/05/17.
 */
import {Test} from '../carbonscore/Test.js';
import {Tests} from '../carbonscore/Tests.js';
//-----
import {traps} from './traps.js';
import {events} from './events.js';
import {checks} from './checks.js';
import config from './../config.json';
// --- utils
import {system} from '../utils/system.js';
//-----
import {DuplicateControl} from '../tag/DuplicateControl.js';
// -1 - bot; 1 - human;

var duplicateControl = new DuplicateControl({
    cookie_key: config.test.cookie_key,
    expiration_day: config.test.expiration_day,
    debug: config.debug
});

var tests_config = config.test.tests,
    source = {
        events: events,
        traps: traps,
        checks: checks,
        getSrc: function (name) {
            var parent = this;
            name.split('.').forEach(function (part) {
                parent = parent[part]
            });
            return parent;
        }
    },
    result = {
        result: config.test.result,
        getRes: function (name) {
            return this.result[name.split('.')[0]];
        }
    };

var eventTests = function () {
    return Object.keys(events).map(function (e) {
        return new Test({
            name: e, src: function (cb) {
                return events[e](cb)
            }
        });
    })
}();

//system.setCookie("cs", "");
//system.setCookie('cs_started', '');

var tests = new Tests({
    tests: Object.keys(tests_config).map(function (e) {
        var test_config = tests_config[e];
        if (test_config.disabled != true
            && duplicateControl.checkStartedTest(test_config.id)
            && duplicateControl.checkCompletedTest(test_config.id)
        ){
            var test = new Test({
                id: test_config.id,
                name: e,
                src: source.getSrc(test_config.src).bind(null, test_config.options),
                result: result.getRes(test_config.src)
            });
            duplicateControl.writeStartedTest(test.id);
            return test;
        }
    })
});

export {tests};
