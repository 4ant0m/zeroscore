import {Events} from '../carbonscore/Events';

var EVENTS = [
    'blur',
    'click',
    'dblclick',
    'focus',
    'focusin',
    'focusout',
    'input',
    'keydown',
    'keypress',
    'keyup',
    'load',
    'mousedown',
    'mouseenter',
    'mouseleave',
    'mousemove',
    'mouseout',
    'mouseover',
    'mouseup',
    'resize',
    'scroll',
    'select',
    'wheel',
    'compositionend',
    'compositionstart',
    'compositionupdate',
    'error',
    'unload',
    'abort',
    'beforeinput',
    'deviceorientation'];

var _events = new Events({EVENTS: EVENTS});

var events = function () {
    var e = {};
    _events.forEach(function (event) {
        e[event.name] = function (options) {
            var cb = arguments[arguments.length - 1];
                event.check.call(event, cb);
        }
    });
    return e;
}();

export {events};
