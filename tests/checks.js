/**
 * Created by 4ant0m on 28.07.17.
 */
import {browser} from '../utils/browser.js';

var checks = {
    checkDNT: function checkDNT(options) {
        var DNT = navigator && navigator.doNotTrack,
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        if (DNT === 1) {
            return cb(DNT);
        }
    },
    checkPlugin: function checkPlugin(options) {
        if (!navigator || !navigator.plugins)
            return;
        var cb = arguments[arguments.length - 1],
            plugins = navigator.plugins;
        cb = cb || function () {
            };

        if (plugins.length > 0)
            return cb(plugins);
    },
    checkBatteryStatus: function checkBatteryStatus(options) {
        var cb = arguments[arguments.length - 1];

        browser.getBattery(function (battery) {
            if (battery)
                return cb(battery);
        });

    },
    checkIndexedDb: function checkIndexedDb(options) {
        var db = browser.getIndexedDb(),
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        if (db.indexedDB) {
            return cb(db);
        }
    }
};

export {checks};