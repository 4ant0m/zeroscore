/**
 * Created by user on 12/05/17.
 */
import {Event} from  '../carbonscore/Event.js';
import {Events} from '../carbonscore/Events.js';
import {browser} from '../utils/browser.js';

var traps = {
    preciseClick: function (options) {
        var cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };
        var click = new Event({name: 'click'}),
            rounding = options && options.rounding,
            topLeft = options && options.topLeft,
            bottomRight = options && options.bottomRight,
            topRight = options && options.topRight,
            bottomLeft = options && options.bottomLeft,
            center = options && options.center;
        click.check(function (e) {
            var src = e.target || e.srcElement;
            var size = {
                    height: src.clientHeight,
                    wight: src.clientWidth
                },
                position = {
                    x: src.clientLeft,
                    y: src.clientTop

                },
                cursor = {
                    x: e.offsetX,
                    y: e.offsetY
                };
            //left top corner
            if (topLeft == true && checkRouding(cursor.x, 0, rounding) && checkRouding(cursor.y, 0, rounding)) {
                return callback(size, position, cursor);
            }
            //right bottom corner
            if (bottomRight == true && checkRouding(cursor.x, size.wight, rounding)
                && checkRouding(cursor.y, size.height, rounding)) {
                return callback(size, position, cursor);
            }
            //right top corner
            if (topRight == true && checkRouding(cursor.x, size.wight, rounding) && checkRouding(cursor.y, 0, rounding)) {
                return callback(size, position, cursor);
            }
            //left bottom corner
            if (bottomLeft == true && checkRouding(cursor.x, 0, rounding) && checkRouding(cursor.y, size.height, rounding)) {
                return callback(size, position, cursor);
            }
            //center
            if (center == true && checkRouding(cursor.x, size.wight / 2, rounding)
                && checkRouding(cursor.y, size.height / 2, rounding)) {
                return callback(size, position, cursor);
            }
        }, true);

        function callback(size, position, cursor) {
            click.clearCheck();
            return cb({
                size: size,
                cursor: cursor,
                position: position
            })
        }

        function checkRouding(position, value, rounding) {
            return Math.abs(position - value) <= rounding
        }
    },
    liedLanguagesKey: function (options) {
        var itemLanguage = navigator && navigator.languages && navigator.languages[0] && navigator.languages[0].substr(0, 2),
            language = navigator && navigator.language && navigator.language.substr(0, 2),
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };
        if ("undefined" !== typeof itemLanguage)
            if (language !== itemLanguage) {
                return cb({language: language, itemLanguage: itemLanguage})
            }
    },
    liedResolutionKey: function (options) {
        if ("undefined" === typeof screen) return;
        var width = screen.width,
            height = screen.height,
            availWidth = screen.availWidth,
            availHeight = screen.availHeight,
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        if (width < availWidth || height < availHeight) {
            return cb({width: width, availWidth: availWidth, height: height, availHeight: availHeight})
        }
    },
    liedOsKey: function (options) {
        var userAgent = navigator && navigator.userAgent && navigator.userAgent.toLowerCase(),
            oscpu = navigator && navigator.oscpu && navigator.oscpu.toLowerCase(),
            platform = navigator && navigator.platform && navigator.platform.toLowerCase(),
            userAgent = 0 <= userAgent.indexOf("windows phone") ?
                "Windows Phone" : 0 <= userAgent.indexOf("win") ?
                "Windows" : 0 <= userAgent.indexOf("android") ?
                "Android" : 0 <= userAgent.indexOf("linux") ?
                "Linux" : 0 <= userAgent.indexOf("iphone") || 0 <= userAgent.indexOf("ipad") ?
                "iOS" : 0 <= userAgent.indexOf("mac") ?
                "Mac" : "Other";
        var cb = arguments[arguments.length - 1],
            res = {
                userAgent: userAgent,
                oscpu: oscpu,
                platform: platform
            };
        cb = cb || function () {
            };


        if (("ontouchstart" in window || 0 < navigator.maxTouchPoints || 0 < navigator.msMaxTouchPoints) &&
            "Windows Phone" !== userAgent && "Android" !== userAgent && "iOS" !== userAgent && "Other" !== userAgent) {
            return cb(res)
        }
        if (typeof oscpu !== 'undefined') {

            if (0 <= oscpu.indexOf("win") && "Windows" !== userAgent && "Windows Phone" !== userAgent) {
                return cb(res)
            }
            if (0 <= oscpu.indexOf("linux") && "Linux" !== userAgent && "Android" !== userAgent) {
                return cb(res);
            }
            if (0 <= oscpu.indexOf("mac") && "Mac" !== userAgent && "iOS" !== userAgent) {
                return cb(res);
            }
            if (0 === oscpu.indexOf("win") && 0 === oscpu.indexOf("linux") && 0 <= oscpu.indexOf("mac") && "other" !== userAgent) {
                return cb(res)
            }
        }

        if (typeof platform !== 'undefined') {
            if (0 <= platform.indexOf("win") && "Windows" !== userAgent && "Windows Phone" !== userAgent) {
                return cb(res)
            }
            if ((0 <= platform.indexOf("linux") || 0 <= platform.indexOf("android") || 0 <= platform.indexOf("pike")) && "Linux" !== userAgent && "Android" !== userAgent) {
                return cb(res);
            }
            if ((0 <= platform.indexOf("mac") || 0 <= platform.indexOf("ipad") || 0 <= platform.indexOf("ipod") || 0 <= platform.indexOf("iphone"))
                && "Mac" !== userAgent && "iOS" !== userAgent) {
                return cb(res);
            }
            if (0 === platform.indexOf("win") && 0 === platform.indexOf("linux") && 0 <= platform.indexOf("mac") && "other" !== userAgent) {
                return cb(res);
            }
        }

        if ("undefined" === typeof navigator.plugins && "Windows" !== userAgent && "Windows Phone" !== userAgent) {
            return cb(res);
        }
        return false;
    },
    heapSize: function heapSize(options) {
        var memory = window && window.performance && window.performance.memory,
            limit = options.limit || 10,
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        if (memory && memory.jsHeapSizeLimit <= limit * 1000000) {
            return cb(memory)
        }
    },
    checkRefer: function checkRefer(options) {
        var refer = document && document.referrer,
            curUrl = window && window.location && window.location.href,
            cb = arguments[arguments.length - 1];
        cb = cb || function () {};

        if (refer == "" || refer == curUrl) {
            return cb(refer);
        }
    },
    checkFullScreen: function checkFullScreen(options) {
        var fullscreenEnabled = document && document.webkitFullscreenEnabled ||
                document.fullscreenEnabled ||
                document.mozFullScreenEnabled ||
                document.msFullscreenEnabled,
            fullScreenMode = checkFullScreenMode(),
            isMobile = browser.checkMobile(),
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        function checkFullScreenMode(options) {
            var fullScreen = window && window.navigator &&
                (window.navigator.standalone || (document.fullScreenElement && document.fullScreenElement !== null)
                || (document.mozFullScreen || document.webkitIsFullScreen) || (screen.width == window.innerWidth && screen.height == window.innerHeight));
            return fullScreen;
        }

        if (fullscreenEnabled && fullScreenMode && !isMobile) {
            return cb({fullscreenEnabled: fullscreenEnabled, fullscreeMode: fullScreenMode})
        }
    },
    checkVendor: function checkVendor(options) {
        var venString = window && window.navigator && window.navigator.vendor,
            browserDetected = browser.checkBrowserVersion(),
            SAFARI = "Apple Computer, Inc.",
            CHROME = "Google Inc.",
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        Object.assign(browserDetected, {vendor: venString});

        if (browserDetected.is.Safari && venString != SAFARI) {
            return cb(browserDetected);
        }
        if (browserDetected.is.Chrome && venString != CHROME) {
            return cb(browserDetected);
        }
    },
    checkProductSub: function checkProductSub(options) {
        var prodSub = window && window.navigator && window.navigator.productSub,
            browserDetected = browser.checkBrowserVersion(),
            SAFARI_CHROME = 20030107,
            MOZILLA = 20100101,
            IE = undefined,
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        Object.assign(browserDetected, {productSub: prodSub});

        if ((browserDetected.is.Safari || browserDetected.is.Chrome) && prodSub != SAFARI_CHROME) {
            return cb(browserDetected);
        }
        if (browserDetected.is.Firefox && prodSub != MOZILLA) {
            return cb(browserDetected);
        }
        if ((browserDetected.is.Edge || browserDetected.is.IE) && prodSub != IE) {
            return cb(browserDetected);
        }
    },
    checkLocalStorage: function checkLocalStorage(options) {
        var cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };
        if (!browser.getLocalStorage())
            return cb();
    },
    checkSessionStorage: function () {
        var cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };
        if (!browser.getSessionStorage())
            return cb();
    },
    mouseKeyboard: function (options) {
        var latency = options && options.latency,
            count = options && options.count,
            cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };
        var MOUSE = 'mouse',
            KEYBOARD = 'keyboard';

        var mousemove = new Event({name: 'mousemove'}),
            mouseclick = new Event({name: 'click'}),
            keyboard = new Event({name: 'keydown'}),
            lastTime = new Date(),
            lastEvent = '',
            n = 0;

        mouseclick.check(function (e) {
            checkSimultaneous(MOUSE);
            lastEvent = MOUSE
        }, true);

        mousemove.check(function (e) {
            checkSimultaneous(MOUSE);
            lastEvent = MOUSE
        }, true);

        keyboard.check(function (e) {
            checkSimultaneous(KEYBOARD);
            lastEvent = KEYBOARD
        }, true);

        function checkSimultaneous(currentEvent) {
            if (!lastEvent) return;
            var speed = new Date().getTime() - lastTime.getTime();
            if (speed <= latency) {
                n++;
                if (n >= count) {
                    mousemove.clearCheck();
                    keyboard.clearCheck();
                    return cb({speed: speed, lastEvent: lastEvent});
                }
            }
            if (lastEvent != currentEvent)
                lastTime = new Date();
        }
    },
    visibilityState: function visibilityState(options) {
        //for old browsers
        var hidden, state, visibilityChange,
            cb = arguments[arguments.length - 1],
            EVENTS = options && options.events || [],
            events = new Events({EVENTS: EVENTS});

        cb = cb || function () {
            };
        if (typeof document.hidden !== "undefined") {
            hidden = "hidden";
            visibilityChange = "visibilitychange";
            state = "visibilityState";
        } else if (typeof document.oHidden !== "undefined") {
            hidden = "oHidden";
            visibilityChange = "ovisibilitychange";
            state = "ovisibilityState";
        } else if (typeof document.mozHidden !== "undefined") {
            hidden = "mozHidden";
            visibilityChange = "mozvisibilitychange";
            state = "mozVisibilityState";
        } else if (typeof document.msHidden !== "undefined") {
            hidden = "msHidden";
            visibilityChange = "msvisibilitychange";
            state = "msVisibilityState";
        } else if (typeof document.webkitHidden !== "undefined") {
            hidden = "webkitHidden";
            visibilityChange = "webkitvisibilitychange";
            state = "webkitVisibilityState";
        }

        if (!hidden || !state || !visibilityChange)
            return;

        var visibility = new Event({name: visibilityChange});
        checkActivity();

        visibility.check(function (e) {
            checkActivity();
        }, true);

        function checkActivity() {
            if (document[hidden] === true) {
                events.check(function (e) {
                    events.flush();
                    visibility.clearCheck();
                    return cb({e: e, visibilityState: document[state]})
                });
            } else {
                events.flush();
            }
        }
    },
    checkWebGL: function checkwebgl(options) {
        var cb = arguments[arguments.length - 1];
        cb = cb || function () {
            };

        var support = browser.checkBrowserSupportByPattern({
            mobile: {
                Safari: 8.0,
                Opera: 12,
                Chrome: 30,
                IE: 11
            },
            pc: {
                Firefox: 4.0,
                Edge: 0,
                Chrome: 9,
                IE: 11,
                Opera: 12,
                Safari: 5.1
            }
        });

        if (!support)
            return;

        function webgl_detect(return_context) {
            if (!!window.WebGLRenderingContext) {
                var canvas = document.createElement("canvas"),
                    names = ["webgl", "experimental-webgl", "moz-webgl", "webkit-3d"],
                    context = false;

                for (var i = 0; i < 4; i++) {
                    try {
                        context = canvas.getContext(names[i]);
                        if (context && typeof context.getParameter == "function") {
                            // WebGL is enabled
                            if (return_context) {
                                // return WebGL object if the function's argument is present
                                return {name: names[i], gl: context};
                            }
                            // else, return just true
                            return true;
                        }
                    } catch (e) {
                    }
                }
                // WebGL is supported, but disabled
                return true;
            }
            // WebGL not supported
            return false;
        }

        var webgl = webgl_detect();
        if (!webgl)
            return cb({webgl: webgl});
    },
    clickPosition: function clickPosition(options) {
        if (browser.isTouchDevice())
            return;

        var pos = {
                x: 0,
                y: 0
            },
            cb = arguments[arguments.length - 1];

        cb = cb || function () {
            };

        var mousemove = new Event({name: 'mousemove'});

        mousemove.check(function (e) {
            pos.x = e.clientX;
            pos.y = e.clientY;
        }, true);

        var click = new Event({name: 'click'});
        click.check(function (e) {
            if (pos.x !== e.clientX && pos.y !== e.clientY) {
                mousemove.clearCheck();
                return cb({pos: pos, e: e});
            }
        }, false)
    },
    checkBrowser: function checkBrowser(options) {
        var cb = arguments[arguments.length - 1];

        cb = cb || function () {
            };

        var browserFeatures = browser.checkBrowser(),
            isMobile = browser.checkMobile(),
            browserVersion = browser.checkBrowserVersion();

        if (isMobile)
            return;

        if ((browserVersion.is.Opera && !browserFeatures.isOpera) ||
            (browserVersion.is.Firefox && !browserFeatures.isFirefox) ||
            (browserVersion.is.Safari && !browserFeatures.isSafari) ||
            (browserVersion.is.Edge && !browserFeatures.isEdge) ||
            (browserVersion.is.Chrome && !browserFeatures.isChrome) ||
            (browserVersion.is.IE && !browserFeatures.isIE) ||
            ((browserVersion.is.Chrome || browserVersion.is.Opera) && !browserFeatures.isBlink) ||
            browserVersion.is.NaN)
            return cb(browserFeatures);
    },
    inLineMove: function inLineMove(options) {
        var positions = {
                xArr: [],
                yArr: []
            },
            err = options && options.err || 0,
            dist = options && options.dist || 10,
            repeat = options && options.repeat || 5,
            cb = arguments[arguments.length - 1];

        cb = cb || function () {
            };

        var mousemove = new Event({name: 'mousemove'});

        mousemove.check(function (e) {
            positions.xArr.push(e.clientX);
            positions.yArr.push(e.clientY);
            if (positions.xArr.length >= 3)
                calculateLine({
                    positions: positions,
                    err: err,
                    dist: dist,
                    repeat: repeat
                }, function (err) {
                    mousemove.clearCheck();
                    return cb({err: err});
                });
        }, true);

        function calculateLine(options, cb) {
            calculateLine.n = calculateLine.n || 0;
            calculateLine.i2 = calculateLine.i2 || 0;
            var i2 = calculateLine.i2;

            var positions = options.positions,
                lastX = positions.xArr.length - 1,
                lastY = positions.yArr.length - 1,
                err = options && options.err,
                dist = options && options.dist,
                repeat = options && options.repeat;

            var x3 = positions.xArr[lastX],
                x2 = positions.xArr[i2],
                x1 = positions.xArr[0];

            var y3 = positions.yArr[lastY],
                y2 = positions.yArr[i2],
                y1 = positions.yArr[0];

            if (i2 == 0 && (Math.abs(x1 - x3) >= dist || Math.abs(y1 - y3) >= dist)) {
                x2 = x3;
                y2 = y3;
                calculateLine.i2 = lastX;
            }

            if ((Math.abs(x1 - x2) < dist || Math.abs(x2 - x3) < dist || Math.abs(x1 - x3) < dist) && (Math.abs(y1 - y2) < dist || Math.abs(y3 - y2) < dist || Math.abs(y1 - y3) < dist)) {
                return;
            }

            var a = (x2 - x1),
                b = (y3 - y1);

            var a1 = (x3 - x1),
                b1 = (y2 - y1);

            var e = a * b - a1 * b1;

            if (Math.abs(e) <= err) {
                calculateLine.n += 1;
                if (calculateLine.n >= repeat)
                    return cb(e)
            } else {
                positions.xArr = [];
                positions.yArr = [];
                calculateLine.i2 = 0;
                calculateLine.n = 0;
            }
        }
    },
    constantSpeedMove: function constantSpeedMove(options) {
        var mousemove = new Event({name: 'mousemove'}),
            n = 0,
            x1 = 0, y1 = 0,
            speedArr = [],
            repeat = options && options.repeat || 5,
            dist = options && options.dist || 2,
            err = options && options.err || 0,
            cb = arguments[arguments.length - 1];

        cb = cb || function () {
            };

        mousemove.check(function (e) {
            x1 = x1 || e.clientX;
            y1 = y1 || e.clientY;
            if (Math.abs(x1 - e.clientX) >= dist || Math.abs(y1 - e.clientY) >= dist) {
                speedArr.push(Math.sqrt(Math.pow((e.clientX - x1), 2) + Math.pow((e.clientY - y1), 2)));

                if (speedArr.length >= 2)
                    if (Math.abs(speedArr[speedArr.length - 2] - speedArr[speedArr.length - 1]) <= err) {
                        n++;
                    } else {
                        speedArr = [];
                        n = 0;
                    }
            }
            if (n >= repeat) {
                mousemove.clearCheck();
                return cb(speedArr);
            }
            x1 = e.clientX;
            y1 = e.clientY;

        }, true)
    },
    actionSpeed: function actionSpeed(options) {
        var latency = options && options.latency || 30,
            EVENTS = options && options.events || [],
            count = options && options.count || 5,
            arrSpeeds = [],
            cb = arguments[arguments.length - 1],
            lastTime = new Date(),
            events = EVENTS.map(function (item) {
                var event = new Event({name: item});
                event.check(function (e) {
                    var speed = new Date().getTime() - lastTime.getTime();
                    arrSpeeds.push(speed);
                    lastTime = new Date();
                    arrSpeeds.averageSpeed = arrSpeeds.reduce(function (prev, cur) {
                            return cur += prev
                        }) / arrSpeeds.length;
                    if (arrSpeeds.length > count && arrSpeeds.averageSpeed < latency) {
                        events.flush();
                        cb(arrSpeeds)
                    } else if (arrSpeeds.length > count)
                        arrSpeeds = []
                }, true);
                return event;
            });
        events.flush = function () {
            this.forEach(function (item) {
                item.clearCheck();
            })
        };
    },
    fileAPISupport: function (options) {
        var cb = arguments[arguments.length - 1],
            FileReader = browser.getFileAPI();

        var support = browser.checkBrowserSupportByPattern({
            mobile: {
                Firefox: 32,
                Edge: 0,
                IE: 10,
                Opera: 11.5,
                Safari: 6.1,
                Android: 3

            },
            pc: {
                Firefox: 3.6,
                Edge: 0,
                Chrome: 7,
                IE: 10,
                Opera: 12.02,
                Safari: 6
            }
        });

        if (support) {
            if (!FileReader) {
                return cb(FileReader);
            }
        }
    }
};

export {traps};
