import {Detection} from './carbonscore/Detection.js';
import {ClickagyInstance} from './tag/ClickagyInstance.js';
import {Fingerprint} from './tag/Fingerprint.js';
import config from './config.json';
import {system} from './utils/system.js';

window._initClickagy = function (clickagyData) {
    if (typeof _clickagyData !== 'undefined') {
        clickagyData = _clickagyData;
    } else if (typeof _platformData !== 'undefined') {
        clickagyData = _platformData;
    }
    var fingerprint = new Fingerprint({
        debug: clickagyData.debug || config.debug,
        options: config.fingerprint.options
    });
    var clickagyInstance = new ClickagyInstance(config),
        cs = clickagyData && clickagyData.cs;

    if (typeof clickagyData.viewabilityAd == 'undefined') {
        clickagyData.viewabilityAd = config.viewabilityAd;
    }

    clickagyInstance.init(clickagyData || {});

    if (cs === true) {
        var detection = new Detection({
            interval_ms: config.interval_ms,
            expiration_day: config.test.expiration_day,
            cookie_key: config.test.cookie_key,
            debug: clickagyData.debug || config.debug
        });
        detection.runTests();
        fingerprint.get(function (fp) {
            detection.getIntervalRes(function (res, writeCookie) {
                var link = clickagyInstance.getAortaUrl() + "/pixel.gif" + res;

                link += '&fp=' + system.encodeUri(fp);

                if (clickagyInstance.u) {
                    link += '&u=' + system.encodeUri(clickagyInstance.u)
                }

                if (clickagyInstance.channel_id) {
                    link += '&ch=' + system.encodeUri(clickagyInstance.channel_id)
                }

                if (clickagyInstance.pub_id) {
                    link += '&pid=' + system.encodeUri(clickagyInstance.pub_id)
                }

                detection.log(link);
                clickagyInstance.loadPixel(link, writeCookie);
            });
        })
    }
};

if (typeof _clickagyData !== "undefined" || typeof _platformData !== "undefined"){
    _initClickagy();
}
