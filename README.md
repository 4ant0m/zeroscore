# Clickagy Data Collection Javascript

## Build

1. Install webpack using: `npm i webpack -g`
2. Build by running: `webpack`

## Usage

```html
<script src="./build/app.js" type="text/javascript"></script>
<script type="text/javascript">
  _initClickagy({
    "cs": true,
    "aid": "3", "list": "11huh75ojm0a29", "ci": true,
    "ch": 8, "pid": "test", "debug": true
  });
</script>
```
