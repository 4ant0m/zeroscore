window.loadScript = function (src, callback) {
    var script = document.getElementsByTagName('script')[0],
        newScript = document.createElement('script');
    // for IE
    newScript.onreadystatechange = function () {
        if (this.readyState === 'loaded' || this.readyState == 'complete') {
            newScript.onreadystatechange = null;
            callback();
        }
    };

    newScript.onload = function () {
        callback();
    };

    newScript.type = 'text/javascript';
    newScript.src = src;

    script.parentNode.insertBefore(newScript, script);
};
