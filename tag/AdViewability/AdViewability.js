/**
 * Created by 4ant0m on 11/9/17.
 */
import {OpenAdViewability} from './OpenAdViewability.js';
import {SafeFrameAdViewability} from './SafeFrameAdViewability.js';
import {system} from '../../utils/system.js';

function AdViewability(options) {
    var self = this;
    this.debug = options.debug || false;

    var check = {
            percentObscured: 0,
            percentViewable: 0,
            acceptedViewablePercentage: options && options.acceptedViewablePercentage || 50,
            acceptedDuration: options && options.acceptedDuration || 1000,
            viewabiltyStatus: false,
            duration: 0,
            DEBUG_MODE: this.debug
        };

    this.log = function () {
        var log = system.log("Clickagy [AdViewability]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };

    /*
     var ads,
     oav;
     this.ads = document.getElementsByClassName('clickagy_ad_container');
     ads = this.ads;
     if (!ads) {
     if (this.debug)
     console.log('Clickagy: Skipping viewability check because unable to locate any ads.');
     return;
     }

     this.oav = new OpenAdViewability();
     oav = this.oav;
     for (var i = 0; i < ads.length; i++) {
     oav.checkViewability(ads[i], function (check) {
     if (check.viewabiltyStatus) {
     if (self.debug) {
     console.log('Clickagy: Viewability triggered!');
     console.log(check);
     }
     self.loadPixel(self.getAortaUrl() + "pixel.gif?vc=1&vc_imp=" + self.data['imp_id']);
     }
     });
     }
     */

    var openAdViewabilty = new OpenAdViewability(options),
        safeFrameAdViewabilty = new SafeFrameAdViewability(options);

    this.checkViewability = function (ad, statusCallback) {
        var count = 0;
        var timer = setInterval(function () {
            var view = self.checkViewable(ad);
            check.percentViewable = view.percentViewable;
            check.percentObscured = view.percentObscured;
            if (check.percentViewable >= check.acceptedViewablePercentage) {
                count++;
            } else {
                count = 0;
            }
            check.duration = count * 100;
            if (check.duration >= check.acceptedDuration) {
                check.viewabiltyStatus = true;
                if (!check.DEBUG_MODE)
                    clearInterval(timer);
            }
            statusCallback(check);
        }, 100);
    };

    this.checkViewabilityAds = function (ad, statusCallback) {
        safeFrameAdViewabilty.register(function () {
            self.checkViewable = safeFrameAdViewabilty.checkViewable() || openAdViewabilty.checkViewable();
            if (safeFrameAdViewabilty.support) {
                return self.checkViewability({}, statusCallback);
            }
            var adBody = system.inIframe() ? document.getElementsByTagName('body')[0] : ad;
            return self.checkViewability(adBody, statusCallback);
        })
    }
}

export {AdViewability}