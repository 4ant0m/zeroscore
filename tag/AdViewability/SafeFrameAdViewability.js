/**
 * Created by 4ant0m on 10/29/17.
 */
import {system} from '../../utils/system.js';
import {browser} from '../../utils/browser.js';

function SafeFrameAdViewability(options) {

    var sfAPI = window.extern || typeof $sf != 'undefined' && $sf.ext || false; // Create a ref to the SafeFrame API
    var self = this;

    this.support = false;

    this.register = function (cb) {
        system.windowOnLoadWithObject(function () {
            setTimeout(function () {
                var bodySize = browser.getBodySize();
                if (!sfAPI) return cb({err: "SafeFrame API doesn't support"});
                    try {
                        // Register status_update listener with initial ad size (leaderboard)
                        sfAPI.register(bodySize.width, bodySize.height, self.status_update);
                        self.support = true;
                        cb (self)
                    } catch (e) {
                        return cb({err: "Exception or no safeframes available: " + e.message});
                    }

            }, 0)
        }, {});
    };

    this.status_update = function (status, data) {
        if (status == "geom-update") {
            self.updateInViewDisplay();
        }
    };

    this.updateInViewDisplay = function () {
        var totalViewable = sfAPI.inViewPercentage();
        return totalViewable;
    };

    this.checkViewable = function () {
        if (self.support) return function () {
            return {
                percentViewable: self.updateInViewDisplay(),
                percentObscured: 0
            }
        };
        return false;
    };

    return this;
}

export {SafeFrameAdViewability}