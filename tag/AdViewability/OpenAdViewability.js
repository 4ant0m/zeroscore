import {OAVGeometryViewabilityCalculator} from './OAVGeometryViewabilityCalculator.js';

function OpenAdViewability(options) {
    /*
     This implementation is according to MRC Viewability guidelines -
     http://mediaratingcouncil.org/081815%20Viewable%20Ad%20Impression%20Guideline_v2.0_Final.pdf
     */

    var geometryViewabilityCalculator = new OAVGeometryViewabilityCalculator();

    var self = this;

    var check = {
        percentObscured: 0,
        percentViewable: 0,
        acceptedViewablePercentage: options && options.acceptedViewablePercentage || 50,
        acceptedDuration: options && options.acceptedDuration || 1000,
        viewabiltyStatus: false,
        duration: 0
    };

    this.checkViewable = function () {
        return function (ad) {
            var adRect = ad.getBoundingClientRect();
            var totalArea = adRect.width * adRect.height;
            // According to MRC standards, larget ad unit size have only 30% viewable requirements
            if (totalArea >= 242500)
                check.acceptedViewablePercentage = 30;

            if (checkCssInvisibility(ad) === true) {
                return false;
            }

            if (checkDomObscuring(ad) === true) {
                return false;
            }

            checkGeometry(ad);

            return check;
        }
    };

    /**
     * Performs the geometry technique to determine viewability. First gathers
     * information on the viewport and on the ad. Then compares the two to
     * determine what percentage, if any, of the ad is within the bounds
     * of the viewport.
     * @param {Element} ad The HTML Element to measure
     */
    var checkGeometry = function (ad) {
        var viewabilityResult = geometryViewabilityCalculator.getViewabilityState(ad, window);
        if (!viewabilityResult.error) {
            check.percentViewable = viewabilityResult.percentViewable - check.percentObscured;
        }
        return viewabilityResult;
    };

    /**
     * Checks if the ad is made invisible by css attribute 'visibility:hidden'
     * or 'display:none'.
     * Is so, viewability at the time of this check is 'not viewable' and no further check
     * is required.
     * These properties are inherited, so no need to parse up the DOM hierarchy.
     * If the ad is in an iframe inheritance is restricted to elements within
     * the DOM of the iframe document
     * @param {Element} ad The HTML Element to measure
     */
    var checkCssInvisibility = function (ad) {
        var style = window.getComputedStyle(ad, null);
        var visibility = style.getPropertyValue('visibility');
        var display = style.getPropertyValue('display');
        if (visibility == 'hidden' || display == 'none') {
            return true;
        }
        return false;
    };

    /**
     * Checks if the ad is more then 50% obscured by another dom element.
     * Is so, viewability at the time of this check is 'not viewable' and no further check
     * is required.
     * If the ad is in an iframe this check is restricted to elements within
     * the DOM of the iframe document
     * @param {Element} ad The HTML Element to measure
     */
    var checkDomObscuring = function (ad) {
        var adRect = ad.getBoundingClientRect(),
            offset = 12,
            xLeft = adRect.left + offset,
            xRight = adRect.right - offset,
            yTop = adRect.top + offset,
            yBottom = adRect.bottom - offset,
            xCenter = Math.floor(adRect.left + adRect.width / 2),
            yCenter = Math.floor(adRect.top + adRect.height / 2),
            testPoints = [
                {x: xLeft, y: yTop},
                {x: xCenter, y: yTop},
                {x: xRight, y: yTop},
                {x: xLeft, y: yCenter},
                {x: xCenter, y: yCenter},
                {x: xRight, y: yCenter},
                {x: xLeft, y: yBottom},
                {x: xCenter, y: yBottom},
                {x: xRight, y: yBottom}
            ];

        for (var p in testPoints) {
            if (testPoints[p] && testPoints[p].x >= 0 && testPoints[p].y >= 0) {
                var elem = document.elementFromPoint(testPoints[p].x, testPoints[p].y);

                if (elem != null && elem != ad && !ad.contains(elem)) {
                    self.overlappingArea = overlapping(adRect, elem.getBoundingClientRect());
                    if (self.overlappingArea > 0) {
                        check.percentObscured = 100 * overlapping(adRect, elem.getBoundingClientRect());
                        if (check.percentObscured > check.acceptedViewablePercentage) {
                            check.percentViewable = 100 - check.percentObscured;
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    };

    var overlapping = function (adRect, elem) {
        var adArea = adRect.width * adRect.height,
            x_overlap = Math.max(0, Math.min(adRect.right, elem.right) - Math.max(adRect.left, elem.left)),
            y_overlap = Math.max(0, Math.min(adRect.bottom, elem.bottom) - Math.max(adRect.top, elem.top));
        return (x_overlap * y_overlap) / adArea;
    }
}

export {OpenAdViewability}