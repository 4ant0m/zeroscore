/**
 * Created by 4ant0m on 12/2/17.
 */
import {system} from '../utils/system.js';
import {browser} from '../utils/browser.js';

function AdGenerator(options) {
    var self = this;
    this.account_id = options && options.account_id;
    this.url = options && options.url;
    this.aorta_url = options && options.aorta_url;
    this.debug = options && options.debug || false;

    this.log = function () {
        var log = system.log("Clickagy [AdGenerator]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };
    this.error = system.log("Clickagy Error [AdGenerator]: ");

}
AdGenerator.prototype.loadAd = function (pos, cb) {
    var self = this,
        urlAppends = "count=3&account_id=" + system.encodeUri(this.account_id),
        adFrame;
    // URL Last
    urlAppends = urlAppends.concat("&u=" + system.encodeUri(this.url));
    this.requestNative(urlAppends, function (ads) {
        if (ads.length < 1) {
            self.error("No ads returned for adGenerator unit.");
            //return;
        }
        if (pos == 'block') {
            adFrame = self.generateBlockAd(ads)
        } else {
            adFrame = self.generateFloatAd(pos, ads);
        }
        if (!adFrame) return self.error("Advertising frame did not generated");
        return cb(self.renderFrame(adFrame));
    });
};

AdGenerator.prototype.requestNative = function (urlAppends, callback) {
    system.xhrGet(this.aorta_url + "/native?" + urlAppends, callback);
};

AdGenerator.prototype.generateBlockAd = function (ads) {
    // Native, fit ad to container
    var adFrame = document.createElement("div");

    adFrame.id = "clickagy-ad-" + Math.floor(Math.random() * 10000000);
    adFrame.style.width = "100%";
    adFrame.style.height = "100%";
    adFrame.style.padding = "0px";
    adFrame.style.margin = "0px";

    this.generateImageAds(adFrame, ads);

    return adFrame;
};

AdGenerator.prototype.generateIframeAd = function (pos, size, cb) {
    var adFrame = document.createElement("iframe"),
        bodySize = browser.getBodySize();

    adFrame.id = "clickagy-ad-" + Math.floor(Math.random() * 10000000);
    adFrame.width = size.width + "px";
    adFrame.height = size.height + "px";
    adFrame.style.padding = "0px";
    adFrame.style.margin = "0px";
    adFrame.frameBorder = 0;
    adFrame.scrolling = "no";
    adFrame.src = this.aorta_url + "/ad?w=" + size.width + "&h=" + size.height;
    adFrame.style.position = "fixed";

    if (pos == "right") {
        adFrame.style.bottom = "10px";
        adFrame.style.right = "10px";
    } else if (pos == "left") {
        adFrame.style.bottom = "10px";
        adFrame.style.left = "10px";
    } else {
        adFrame.style.left = bodySize.width/2 - size.width/2;
    };

    return cb(this.renderFrame(adFrame));
};

AdGenerator.prototype.generateFloatAd = function (pos, ads) {
    var adFrame = document.createElement("div"),
        self = this;

    if (!browser.checkScreenSize(1024, 500)) {
        self.error('Screen size too small for a Float ad.');
        return
    }
    adFrame.id = "clickagy-ad-" + Math.floor(Math.random() * 10000000);
    adFrame.style.backgroundColor = "#383838";
    adFrame.style.fontFamily = "helvetica,arial,sans-serif";
    adFrame.style.color = "#fff";
    adFrame.style.position = "fixed";

    if (pos == "right") {
        adFrame.style.bottom = "10px";
        adFrame.style.right = "10px";
    } else if (pos == "left") {
        adFrame.style.bottom = "10px";
        adFrame.style.left = "10px";
    } else {
        self.error("Invalid float ad position: " + pos);
        return;
    }

    adFrame.style.width = "200px";
    // adFrame.style.height = "500px";
    adFrame.style.padding = "0px";
    adFrame.style.margin = "0px";
    adFrame.style.border = "1px solid #444";
    adFrame.style.borderRadius = "4px";
    adFrame.style.overflow = "hidden";
    adFrame.style.scrolling = "no";

    adFrame.innerHTML = '<style type="text/css">' +
        '.' + adFrame.id + 'float_native_ad { padding: 10px;display:block;color:#fff;text-decoration: none; }' +
        '.' + adFrame.id + 'float_native_ad:hover,.' + adFrame.id + 'float_native_ad:active { background-color:#444; } ' +
        '.' + adFrame.id + 'float_native_ad .headline { font-size: 12pt; }' +
        '.' + adFrame.id + 'float_native_ad .headline span { padding-left:6px;font-size:9pt;color: #595; }' +
        '.' + adFrame.id + 'float_native_ad .creative { height: 90px; width: 100%; }' +
        '.' + adFrame.id + 'float_native_footer { font-size:10px;color:#bbb;position:absolute;bottom:5px;right:5px; }' +
        '.' + adFrame.id + 'float_native_footer a,.' + adFrame.id + 'float_native_footer a:hover,.' + adFrame.id + 'float_native_footer a:active { color:#ccc;text-decoration: none; }' +
        '.clickagy-pixel { position:absolute;left:-999px;top:-999px; }' +
        '</style>';

    this.generateImageAds(adFrame, ads);

    return adFrame;
};

AdGenerator.prototype.generateImageAds = function (adFrame, ads) {
    adFrame.innerHTML = '<div style="background-color:#666;color:#eee;font-size: 9pt;font-weight:bold;padding:4px;margin-bottom:10px;"><a style="float:right" href="javascript:;" onclick="javascript:document.getElementById(\'' + adFrame.id + '\').style.display = \'none\';"><img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+Cjxzdmcgd2lkdGg9IjEycHgiIGhlaWdodD0iMTJweCIgdmlld0JveD0iMCAwIDEyIDEyIiB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPgogICAgPCEtLSBHZW5lcmF0b3I6IFNrZXRjaCAzLjcuMiAoMjgyNzYpIC0gaHR0cDovL3d3dy5ib2hlbWlhbmNvZGluZy5jb20vc2tldGNoIC0tPgogICAgPHRpdGxlPmNsb3NlPC90aXRsZT4KICAgIDxkZXNjPkNyZWF0ZWQgd2l0aCBTa2V0Y2guPC9kZXNjPgogICAgPGRlZnM+PC9kZWZzPgogICAgPGcgaWQ9IlBhZ2UtMSIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgaWQ9IkFydGlzdC1QYWdlLU1vZGFsLS0tTm8tTG9jYWwiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC03MTQuMDAwMDAwLCAtMjA3LjAwMDAwMCkiIGZpbGw9IiNDN0M3QzciPgogICAgICAgICAgICA8ZyBpZD0iTGlnaHRib3giIHRyYW5zZm9ybT0idHJhbnNsYXRlKDMzNC4wMDAwMDAsIDE5OS4wMDAwMDApIj4KICAgICAgICAgICAgICAgIDxwYXRoIGQ9Ik0zODcuODU3MzM0LDE0IEwzOTEuOTI2NzM0LDkuOTMwNzA2NjkgQzM5Mi4wMjQ0MjIsOS44MzMwMjA2NCAzOTIuMDI0NDIyLDkuNjc0NjcxNjcgMzkxLjkyNjczNCw5LjU3Njk4NTYyIEwzOTAuNDIzMjU1LDguMDczMjk1ODEgQzM5MC4zNzYzNDksOC4wMjYzOTE0OSAzOTAuMzEyNjgzLDggMzkwLjI0NjM5LDggQzM5MC4xODAwOTYsOCAzOTAuMTE2NDMsOC4wMjYzOTE0OSAzOTAuMDY5NTI0LDguMDczMjk1ODEgTDM4NiwxMi4xNDI3MTQyIEwzODEuOTMwNDc2LDguMDczMjk1ODEgQzM4MS44MzY2NjQsNy45Nzk0ODcxOCAzODEuNjcwNTU2LDcuOTc5NDg3MTggMzgxLjU3Njc0NSw4LjA3MzI5NTgxIEwzODAuMDczMjY2LDkuNTc2OTg1NjIgQzM3OS45NzU1NzgsOS42NzQ2NzE2NyAzNzkuOTc1NTc4LDkuODMzMDIwNjQgMzgwLjA3MzI2Niw5LjkzMDcwNjY5IEwzODQuMTQyNjY2LDE0IEwzODAuMDczMjY2LDE4LjA2OTI5MzMgQzM3OS45NzU1NzgsMTguMTY2OTc5NCAzNzkuOTc1NTc4LDE4LjMyNTMyODMgMzgwLjA3MzI2NiwxOC40MjMwMTQ0IEwzODEuNTc2NzQ1LDE5LjkyNjU3OTEgQzM4MS42MjM2NTEsMTkuOTczNDgzNCAzODEuNjg3MzE3LDE5Ljk5OTg3NDkgMzgxLjc1MzYxLDE5Ljk5OTg3NDkgQzM4MS44MTk5MDQsMTkuOTk5ODc0OSAzODEuODgzNTcsMTkuOTczNDgzNCAzODEuOTMwNDc2LDE5LjkyNjU3OTEgTDM4NiwxNS44NTcyODU4IEwzOTAuMDY5NTI0LDE5LjkyNjcwNDIgQzM5MC4xMTY0MywxOS45NzM2MDg1IDM5MC4xODAwOTYsMjAgMzkwLjI0NjM5LDIwIEMzOTAuMzEyNjgzLDIwIDM5MC4zNzYzNDksMTkuOTczNjA4NSAzOTAuNDIzMjU1LDE5LjkyNjcwNDIgTDM5MS45MjY3MzQsMTguNDIzMTM5NSBDMzkyLjAyNDQyMiwxOC4zMjU0NTM0IDM5Mi4wMjQ0MjIsMTguMTY3MTA0NCAzOTEuOTI2NzM0LDE4LjA2OTQxODQgTDM4Ny44NTczMzQsMTQgTDM4Ny44NTczMzQsMTQgWiIgaWQ9ImNsb3NlIj48L3BhdGg+CiAgICAgICAgICAgIDwvZz4KICAgICAgICA8L2c+CiAgICA8L2c+Cjwvc3ZnPg==" border="0" alt="Close"></a>Recommended Content</div>';
    for (var i = 0; i < ads.length; i++) {
        if (!system.hasOwnProperty(ads[i], "headline") || !system.hasOwnProperty(ads[i], "image_url") || !system.hasOwnProperty(ads[i], "impression_url"))
            continue;

        adFrame.innerHTML += "<a class='" + adFrame.id + "float_native_ad' target='_blank' href='" + ads[i]["click_url"] + "' >" +
            "<img class='creative' src='" + ads[i]["image_url"] + "' border='0' alt='' />" +
            "<div class='headline'>" + ads[i]["headline"] + " <span>" + ads[i]["display_url"] + "</span></div>" +
            "</a>" +
            "<img class='clickagy-pixel' src='" + ads[i]["impression_url"] + "' border='0' height='1' width='1' />";
    }
    adFrame.innerHTML += "<br><div class='" + adFrame.id + "float_native_footer'><a href='https://www.clickagy.com/company/privacy' target='_blank' rel='nofollow'>Privacy</a> &nbsp;|&nbsp; <a href='https://www.clickagy.com' target='_blank'>Clickagy</a></div>";

    return adFrame;
};

AdGenerator.prototype.renderFrame = function (frame) {
    document.body.appendChild(frame);
    return document.getElementById(frame.id);
};

export {AdGenerator};