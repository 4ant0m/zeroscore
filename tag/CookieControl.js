/**
 * Created by 4ant0m on 1/25/18.
 */
import {system} from '../utils/system.js';

function CookieControl(data) {
    var self = this;

    this.debug = data.debug;
    this.cookie_key = data.cookie_key || 'cs';
    this.expiration_day = 1000 * 3600 * 24 * data.expiration_day;

    this.log = function () {
        var log = system.log("Clickagy [CookieControl]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };
};

CookieControl.prototype.writeCookie = function (results) {
    var cookie = JSON.stringify(results);
    system.setCookie(this.cookie_key, cookie);
};

CookieControl.prototype.parseCookie = function () {
    var cookie = system.readCookie(this.cookie_key);
    if (cookie == null)
        return {};
    try {
        return JSON.parse(cookie);
    } catch (e) {
        return {}
    }
};

export {CookieControl};