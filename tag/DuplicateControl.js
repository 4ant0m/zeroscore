/**
 * Created by 4ant0m on 12/17/17.
 */
import {system} from '../utils/system';
import {CookieControl} from './CookieControl.js'

function DuplicateControl(data) {
    var self = this;
    this.debug = data.debug;

    this.cs_completed_tests = new CookieControl({
        cookie_key: data.cookie_key,
        expiration_day: data.expiration_day,
        debug: self.debug
    });

    this.cs_started_tests = new CookieControl({
        cookie_key: 'cs_started',
        expiration_day: 0.0001,
        debug: self.debug
    });

    this.completed_tests = this.cs_completed_tests.parseCookie() || {};
    this.started_tests = this.cs_started_tests.parseCookie() || {};

    if (typeof window._clickagyInstanceCount == 'undefined') {
        window._clickagyInstanceCount = 1;
    } else
        window._clickagyInstanceCount += 1;

    this.count = window._clickagyInstanceCount;

    this.log = function () {
        var log = system.log("Clickagy [DuplicateControl]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };

    //window.top.document
    //iframeObject.contentWindow
    var cookie = system.readCookie("duplicate_test");
    system.setCookie('duplicate_test', cookie - 1);
    // console.log(system.readCookie("duplicate_test"));
}

DuplicateControl.prototype.filterUnCompletedTests = function (results) {
    var self = this;

    results.filter(function (result) {
        if (self.checkCompletedTest(result.id)) {
            self.completed_tests[result.id] = Date.now();
            return true
        }
        return false;
    });
    self.log('Completed Tests: ', self.completed_tests);
    return results;
};

DuplicateControl.prototype.checkCompletedTest = function (testId) {
    return this.checkTestExpiration(testId, 'completed_tests');
};

DuplicateControl.prototype.checkStartedTest = function (testId) {
    this.started_tests = this.cs_started_tests.parseCookie() || {};
    return this.checkTestExpiration(testId, 'started_tests');
};

DuplicateControl.prototype.checkTestExpiration = function (testId, tests) {
    var expireDate = this[tests][testId] + this['cs_' + tests]["expiration_day"];

    if (this[tests][testId] == null ||
       expireDate <= Date.now()) {
        return true;
    } else
        return false;
};

DuplicateControl.prototype.writeCompletedTests = function () {
    this.cs_completed_tests.writeCookie(this.completed_tests);
};

DuplicateControl.prototype.writeStartedTest = function (testId) {
    if (this.checkStartedTest(testId)){
        this.started_tests[testId] = Date.now();
        this.cs_started_tests.writeCookie(this.started_tests);
        this.log('Started Tests: ', this.started_tests);
    }
};

export {DuplicateControl}