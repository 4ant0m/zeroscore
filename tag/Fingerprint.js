import {browser} from '../utils/browser.js';
import {system} from '../utils/system.js';
import * as fingerprint from '../utils/fingerprint';

function Fingerprint(data) {
    this.debug = data && data.debug || false;
    this.result = {};
    this.fp = '';
    this.options = data && data.options || {}
};

Fingerprint.prototype.collect = function (callback) {
    var self = this,
        semaphore = 0,
        info = browser.collectBrowserInfo();

    this.result = {
        userAgent: '',
        language: '',
        colorDepth: '',
        mimeTypes: '',
        hardwareConcurrency: '',
        listOfPlugins: '',
        indexedDB: '',
        platform: '',
        cookiesPreference: '',
        DNT: '',
        timezone: '',
        screenResolution: '',
        localStorage: '',
        sessionStorage: '',
        canvas: '',
        webgl: '',
        fonts: '',
        characterSet: '',
        battery: ''
    };

    this.result.userAgent = info.userAgent;
    this.result.language = info.language;
    this.result.colorDepth = info.colorDepth;
    this.result.mimeTypes = browser.getMimeTypes();
    this.result.hardwareConcurrency = info.hardwareConcurrency;
    this.result.listOfPlugins = browser.getListOfPlugins();
    this.result.indexedDB = browser.getIndexedDb().indexedDB ? true : false;
    this.result.platform = info.platform;
    this.result.cookiesPreference = browser.getCookiePreference();
    this.result.DNT = info.DNT;
    this.result.timezone = info.timezone;
    this.result.screenResolution = info.screenResolution.width * info.screenResolution.height;
    this.result.localStorage = browser.getLocalStorage() ? true : false;
    this.result.sessionStorage = browser.getSessionStorage() ? true : false;
    this.result.canvas = fingerprint.canvas.getFp();
    this.result.webgl = fingerprint.webgl.getFp();
    async(fingerprint.font.getFonts, function (fonts) {
        self.result.fonts = fonts;
    });
    this.result.characterSet = info.characterSet;
    async(browser.getBattery, function (battery) {
        self.result.battery = battery ? true : false;
    });

    function async(func, cb) {
        if (!self.semaphore) self.semaphore = 0;
        if (!self.SEMAPHORE) self.SEMAPHORE = 0;
        self.SEMAPHORE++;

        func.call(this, function (res) {
            cb(res);
            self.semaphore++;
            if (self.semaphore == self.SEMAPHORE) {
                return callback(self.result);
            }
        });
    }

};

Fingerprint.prototype.sign = function () {
    var resultStr = JSON.stringify(this.result);
    this.log('Fingerprint data', this.result);
    this.fp = system.MD5(resultStr);
    return this.fp;
};

Fingerprint.prototype.get = function (cb) {
    var self = this;
    this.collect(function () {
        self.filter();
        cb(self.sign());
    })
};

Fingerprint.prototype.log = function () {
    if (this.debug === true) {
        console.log.apply(console, arguments);
    }
};

Fingerprint.prototype.filter = function () {
    for (var param in this.options) {
        if (this.options.hasOwnProperty(param) && this.options[param] == false) {
            delete this.result[param]
        }
    }
};

export {Fingerprint};