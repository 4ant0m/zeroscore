/*
 VALID DATA OPTIONS:
 *    - dc  (true/false, for aggressive data collection, default false)
 *    - aid (advertiser ID or account ID, passed through on all requests, whenever present)
 *    - pub_id (DEPRECATED - publisher ID - should now alias as "aid")
 *    - list  (for retargeting, passed through on initial request after page load)
 *    - sub_id  (DEPRECATED - requires aid)
 *    - c  (Campaign ID - if set passed through on initial request after page load)
 *    - ag  (Ad Group ID  - if set passed through on initial request after page load)
 *    - a  (Impression Ad ID - if set passed through on initial request after page load)
 *    - t (Impression Targeting - if set passed through on initial request after page load)
 *    - conv_v (Conversion value - if set passed through on initial request after page load)
 *    - k  (DEPRECATED - Untranslated Category/Context)
 *    - u  (Current URL, if set, passed through on initial request after page load.  If not set, tag should try to figure out top-level page URL and pass that through as "u" on initial request.)
 *    - ch (Channel ID.  If set, pass through on all requests except for email capturing [that always sends ch=97 unless overridden])
 *    - email_ch (Email Channel ID.  If set, override default channel "97" used on email capturing)
 *    - fc (Force Crawl, if set, pass through on initial request after page load)
 *    - request_geo (DEPRECATED - ask user for lat/long)
 *    - trigger{}  (array of trigger criteria, and values to send AORTA once fired)
 *                 Example:  [ { 'type': 'sec', 'val': 5, 'execute': { 'list': 'abc', 'conv_v': 25 } ]
 *                 Example:  [ { 'type': 'session_pages', 'val': 2, 'execute': { 'list': 'abc', 'conv_v': 25 } ]
 *                 Example:  [ { 'type': 'onclick', 'val': 'register', 'execute': {'list': 'abc', 'conv_v': 25}  }]
 *                   'type', 'val', and 'execute' are required fields.
 *                   'type' should be 'sec' or 'session_pages'.
 *                   'execute' is what is sent to AORTA after the delay is completed.
 *    - ad (publisher ads - can be defined in JS, but ultimately returned from AORTA based on account settings)
 *         Example:
 *         "ad": {
 *              "w": 300,
 *              "h": 250,
 *              "type": "native", //"standard", "native-float-bottom-left" "native-float-bottom-right"
 *               "pos": "block" //right, left
 *          }
 */

import {AdViewability} from './AdViewability/AdViewability.js';
import {AdGenerator} from './AdGenerator.js';
import {system} from '../utils/system.js';
import {browser} from '../utils/browser.js';

function ClickagyInstance (config) {
    var self = this;

    this.version = '2.0.0';
    this.debug = false;
    this.dc = false;
    this.publisher = false;
    this.exchange = false;
    this.channel_id = null;
    this.pub_id = null;
    this.email_ch = null;
    this.account_id = false;
    this.data = {};
    this.clickActions = false;
    this.bountyVars = [];
    this.isSecure = false;
    this.emailHashCapture = false;
    this.urlGetParams = null;
    this.doneCookieMatching = false;
    this.inIFrame = false;
    this.currentDomain = null;
    this.browserWidth = 0;
    this.browserHeight = 0;
    this.viewabilityAd = null;

    this.window = browser.window;

    this.log = function () {
        var log = system.log("Clickagy [ClickagyInstance]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };

    this.error = system.log("Clickagy Error [ClickagyInstance]: ");
    this.config = config;

    this.init = function (inData) {
        if (typeof inData !== "object") {
            this.error("Invalid configuration!  Pixel deactivated.");
            this.data = {};
        } else {
            // Make copy of configuration
            this.data = JSON.parse(JSON.stringify(inData));
        }
        this.getAortaUrl = system.getUrl(this.config.url.aorta);
        this.getPortalUrl = system.getUrl(this.config.url.portal);
        this.dataHasProperty = system.dataHasProperty(this.data);

        // Debug Tag
        if (this.dataHasProperty('debug')) {
            this.debug = (this.data.debug == true);
            delete this.data.debug;
        }

        if (this.dataHasProperty('viewabilityAd')) {
            this.viewabilityAd = this.data.viewabilityAd;
            delete this.data.viewabilityAd;
        }

        // Is an Exchange?
        if (this.dataHasProperty('exchange')) {
            this.exchange = (this.data.exchange == true);
            delete this.data.exchange;
        } else {
            // Try to detect
            if (this.dataHasProperty('imp_id') || location.search.split('&bid=')[1] != undefined)
                this.exchange = true;
        }

        var urlParams = system.parseUrlGetParams(location.search);
        this.channel_id = urlParams['clickagy_ch'] || urlParams['ch'] || this.data.ch || null;
        this.pub_id = urlParams['clickagy_pid'] || urlParams['pid'] || this.data.pid || null;

        if (this.dataHasProperty('email_ch')) {
            this.email_ch = this.data.email_ch;
            delete this.data.email_ch;
        }

        this.resetBounty();
        this.isSecure = (location.protocol == 'https:');
        this.currentDomain = system.getDomain();
        if (this.exchange || this.currentDomain.substr(this.currentDomain.length - 12) == 'clickagy.com')  // Any Clickagy subdomain, or over RTB
            this.currentDomain = 'clickagy.com';


        // Tag loading alongside an RTB ad - check viewability!
        // if(this.exchange)

        // Associate physical location with an IP address
        if (this.dataHasProperty('request_geo')) {
            this.requestGeo();
            delete this.data.request_geo;
        }

        // CC/Keating Override
        if (this.dataHasProperty('keating') || this.dataHasProperty('cc')) {
            this.data.dc = true;
        }

        // Data Collection
        if (this.dataHasProperty('dc')) {
            if (this.data.dc == true) {
                system.attachEvent(this.window, "load", self.collectFormData);
                self.log('DC: enabled');
            }
            this.dc = this.data.dc;
            delete this.data.dc;
        }

        // Custom User Info (Parse JS object to flat)
        if (this.dataHasProperty('info') && typeof this.data.info == "object") {
            for (var key in this.data.info)
                this.bountyAppend("info~" + key, this.data.info[key]);
            delete this.data.info;
        }

        // Collect other browser info
        this.collectBrowserInfo();

        // Clickagy Insights
        if (this.data.ci !== false) {
            this.loadClickagyInsights();
        }

        // Email Addresses & Email Hashes
        if (this.dataHasProperty('email') || this.dataHasProperty('email_md5') || this.dataHasProperty('email_sha1') || this.dataHasProperty('email_sha256')) {
            var ptEmail = '';
            if (this.dataHasProperty('email'))
                ptEmail = this.data['email'];
            this.ingestEmail(ptEmail);
            delete this.data['email'];
            delete this.data['email_sha1'];
            delete this.data['email_sha256'];
            delete this.data['email_md5'];
        }

        if (this.dataHasProperty('trigger')) {
            this.initTriggerActions(this.data['trigger']);
            delete this.data['trigger'];
        }

        if (this.dataHasProperty("aid") || this.dataHasProperty("account_id") || this.dataHasProperty("advertiser_id")) {
            this.account_id = this.data["aid"] || this.data['account_id'] || this.data['advertiser_id'];
        }

        // Load Ads
        if (this.dataHasProperty('ad')) {
            this.loadAds(function (ad) {
                self.initViewability(self.viewabilityAd, ad);
            });
            delete this.data['ad'];
        }

        // Load Facebook
        if (this.dataHasProperty('list'))
            this.loadPixel('https://www.facebook.com/tr?id=1710397319260964&ev=list&cd[list_id]=' + this.data['list']);

        // Loading inside an iFrame?
        try {
            this.inIFrame = window.self !== window.top;
        } catch (e) {
            this.inIFrame = true;
        }

        // Always send the URL of the current page and referring page
        if (this.inIFrame && window.referrer && window.referrer != "undefined") {
            this.u = window.referrer;
        }
        else if (this.inIFrame && document.referrer && document.referrer != "undefined") {
            this.u = document.referrer;
        }
        else {
            this.u = window.location.href;
        }
        if (!this.inIFrame)
            this.bountyAppend("ref", document.referrer);

        if (this.dataHasProperty('u')) {
            this.u = this.data['u']
        }
        this.bountyAppend("url", this.u);

        // Check for client-specific overrides
        this.checkClientOverrides();

        // Pass-through all variables
        for (var key in this.data) {
            if (this.dataHasProperty(key)) {
                if (key == "aid")  // Alias
                    this.bountyAppend("account_id", this.data[key]);
                else
                    this.bountyAppend(key, this.data[key]);
            }
        }
        // Ready to send bounty?
        this.sendBounty();

        // Post bounty processing (less important things)
        this.secondaryProcessing();

        // Hardcode for "tomballford.com" or "billluke.com"
        var domainParts = this.currentDomain.split('.');
        if (domainParts[domainParts.length - 2] == "tomballford" || domainParts[domainParts.length - 2] == "billluke") {
            this.loadPixel("https://p.alocdn.com/c/xz7dxtun/a/etarget/p.gif?label=" + this.data["b"]);
        }

        self.log('CurrentInstance', this);
    },
        this.collectFormData = function () {
            system.attachEvent(this.window, "change", function (event) {
                self.sendFormData.call(event.target)
            });
        },
        this.monitorEmailHashInputs = function () {
            system.attachEvent(this.window, "change", function (event) {
                self.processCIData.call(event.target);
            });
        },
        this.scanEmailHashInputs = function () {
            var inputs = document.getElementsByTagName('input');

            for (var i = 0; i < inputs.length; i++) {
                self.processCIData.call(inputs[i]);
            }
        },
        this.processCIData = function () {
            var v = this.value.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            if (!system.isValidEmail(v))
                return;

            self.ingestEmail(v);
        },
        this.secondaryProcessing = function () {
            if (this.channel_id != "8")  { // AdX
                this.ipHashLoad();
            }
        },
        this.ipHashLoad = function () {
            var url = self.getPortalUrl() + "/external/hasHashes";
            system.xhrGet(url, this.ipHashLoadResults)
        },
        this.ipHashLoadResults = function (response) {
            if (!(response instanceof Array))
                return;
            if (response.length < 1)
                return;

            // Should only return one now, but just in case ...
            for (var i = 0; i < response.length; i++) {
                self.data['ch'] = '134';
                self.data['email_md5'] = response[i];
                self.ingestEmail();
                if (i > 25) // Max 25 hashes per IP
                    break;
            }
        },
        this.ingestEmail = function (ptEmail) {
            var url = self.getPortalUrl() + '/external/emailHash?';
            if (ptEmail) {
                if (self.email_ch) {
                    url += 'ch=' + system.encodeUri(self.email_ch) + '&'
                } else
                    url += 'ch=97&';
            }
            else if (self.dataHasProperty('ch'))
                url += 'ch=' + system.encodeUri(self.data['ch']) + '&';
            if (self.dataHasProperty('email_md5'))
                url += 'email_md5=' + system.encodeUri(self.data['email_md5']) + '&';
            if (self.dataHasProperty('email_sha1'))
                url += 'email_sha1=' + system.encodeUri(self.data['email_sha1']) + '&';
            if (self.dataHasProperty('email_sha256'))
                url += 'email_sha256=' + system.encodeUri(self.data['email_sha256']) + '&';
            if (ptEmail)
                url += 'email64=' + system.base64Encode(ptEmail) + '&';
            url = url.replace(/&+$/, '');

            this.loadIFrame(url, 0, 0, true, null);
        },
        this.checkClientOverrides = function () {
            // Client Command
            this.checkClientOverrideClientCommand();
        },
        this.checkClientOverrideClientCommand = function () {
            if (!this.dataHasProperty('cc') || this.data.cc != true)
                return;

            // Set user info variable for language
            this.bountyAppend("info~lang", navigator.language);

            // UTM Tags
            this.parseUrlGetParams();
            for (var key in this.urlGetParams) {
                if (key.substr(0, 4) == "utm_")
                    this.bountyAppend("info~" + key, this.urlGetParams[key]);
            }
        },
        this.sendFormData = function () {
            // Save to user info
            var url = '';
            if (!this.name || !this.value)
                return;  // No data

            if (!self.data["aid"]) {
                self.log("Skipping data recording because no aid was specified!");
                return;
            }
            url = self.getAortaUrl();
            url += "/pixel.gif?advertiser_id=" + self.data["aid"];
            url += "&info~" + system.encodeUri("F:" + this.name.toLowerCase().substr(0, 30)) + "="
                + system.encodeUri(this.value);

            self.loadPixel(url);

            self.log("Clickagy: Recorded name '" + this.name + "' with value '" + this.value + "'");
        },
        this.loadAds = function (cb) {
            if (!this.account_id) {
                self.error('Missing or non-valid account ID!');
                return;
            }
            var adGenerator = new AdGenerator({
                account_id: this.account_id,
                aorta_url: this.getAortaUrl(),
                url: this.data["u"] || window.location.href
            });

            // native ads
            if (this.data['ad']['type'] == 'native') {
                adGenerator.loadAd('block', cb);
                return;
            }

            if (this.data['ad']['type'] == 'native' && this.data['ad']['pos'] == 'right') {
                adGenerator.loadAd('right', cb);
                return;
            }

            if (this.data['ad']['type'] == 'native' && this.data['ad']['pos'] == 'left') {
                adGenerator.loadAd('left', cb);
                return;
            }

            // iframe ads
            if (this.data['ad']['type'] == 'standard') {
                adGenerator.generateIframeAd(this.data['ad']['type']['pos'], {
                    width: this.data['ad']['w'],
                    height: this.data['ad']['h']
                }, cb);
            }
        },

        //vc = 1 - viewability was detected, vc = 0 - cant detect viewabilty
        this.initViewability = function (options, ad) {
            // First locate the ad within this iframe
            var oav;

            if (!system.canAccessToParent()) {
                this.log("Viewability check disabled, can't access to parent object");
                self.loadPixel(self.getAortaUrl() + "/pixel.gif?vc=0&vc_imp=" + self.data['imp_id']);
                return;
            }

            this.oav = new AdViewability(options);
            oav = this.oav;

            if (!ad) {
                this.log('Skipping viewability check because unable to locate any ads.');
                return;
            }

            oav.checkViewabilityAds(ad, function (check) {
                if (check.err) return self.error(check.err);
                if (check.DEBUG_MODE) self.log('Viewability status', check);
                if (check.viewabiltyStatus) {
                    self.log('Viewability triggered!', check);
                    self.loadPixel(self.getAortaUrl() + "/pixel.gif?vc=1&vc_imp=" + self.data['imp_id']);
                }
            });
        },
        this.parseUrlGetParams = function () {
            if (this.urlGetParams != null)
                return;  // Already parsed
            this.urlGetParams = system.parseUrlGetParams(location.search);
            return;
        },
        this.craftUrlGetParams = function (assocArray) {
            if (!(assocArray instanceof Object))
                return '';

            var output = [];
            for (var key in assocArray)
                output.push(system.encodeUri(key) + "=" + system.encodeUri(assocArray[key]));
            return output.join("&");
        },
        this.bountyAppend = function (key, val) {
            this.bountyVars.push(key + "=" + system.encodeUri(val));
        },
        this.resetBounty = function () {
            this.bountyVars = [];
            this.loadBeacon();
        },
        this.sendBounty = function () {
            system.xhrPost(this.getAortaUrl() + "/data", this.bountyVars.join("&"), this.parseResponse);
        },
        this.loadBeacon = function () {
            // First check request variable
            if (this.dataHasProperty("b") && typeof this.data["b"] == "string" && this.data["b"].length > 10) {
                this.setBeacon(this.data["b"]);
                this.log("Beacon [B]: " + this.data["b"]);
            } else {
                this.data["b"] = '';  // No local override, so remove any potentially malformed "b" (such as a non-string)
            }

            // Next Check Cookies
            if ((this.debug || !this.data["b"]) && document.cookie.indexOf("cb=") >= 0) {
                this.setBeacon(system.readCookie("cb"));

                this.log("Beacon [C]: " + this.data["b"]);
            }
        },
        this.loadClickagyInsights = function () {
            self.monitorEmailHashes();
            self.log("Insights: enabled");
        },
        this.monitorEmailHashes = function () {
            if (this.emailHashCapture == true)
                return;

            system.attachEvent(this.window, "load", self.monitorEmailHashInputs);
            system.attachEvent(this.window, "unload", self.scanEmailHashInputs);
            this.emailHashCapture = true;
        },
        this.setBeacon = function (beacon) {
            if (beacon == "null" || beacon == "NaN" || !beacon)
                return;

            this.data["b"] = beacon;

            // Add to bounty (if first load on Set Beacon - multiple loads possible via PNG and such)
            if (this.bountyVars.indexOf("b=" + this.data["b"]) < 0 && this.data["b"] != "")
                this.bountyAppend("b", this.data["b"]);

            // Set Standard Cookies
            system.setCookie("cb", this.data["b"]);
        },
        this.getBeacon = function () {
            if (!self.data["b"]) {
                self.error('Could not locate a beacon!');
                return null;
            }
            return self.data["b"];
        },
        this.parseResponse = function (response) {
            if (typeof response['sc'] === "string" && response['sc'].length > 0)
                system.setCookie('clickagy', response['sc']);
            if (typeof response["b"] === "string" || typeof response["b"] === "number") {
                self.setBeacon(response["b"]);
            } else {
                throw new Error("Clickagy Error: " + response);
            }
        },
        this.loadTagFrame = function (url) {
            // Load 3rd-Party CLU Tags iframe
            var tagFrame = document.createElement("IFRAME"),
                first = document.getElementsByTagName('script')[0];  // http://www.jspatterns.com/the-ridiculous-case-of-adding-a-script-element/

            tagFrame.setAttribute("src", url);
            tagFrame.style.width = "0px";
            tagFrame.style.height = "0px";
            tagFrame.style.display = "none";

            first.parentNode.insertBefore(tagFrame, first);
        },
        this.loadIFrame = function (url, height, width, hidden, fallback) {
            var tagFrame = document.createElement("IFRAME");

            if (hidden || (height < 1 && width < 1))
                tagFrame.style.display = "none";
            if (!isNaN(width) && isFinite(width))
                width = width + "px";
            if (!isNaN(height) && isFinite(height))
                height = height + "px";

            tagFrame.marginHeight = "0";
            tagFrame.marginWidth = "0";
            tagFrame.style.width = width;
            tagFrame.style.height = height;
            tagFrame.style.padding = "0px";
            tagFrame.style.margin = "0px";
            tagFrame.style.border = "0px";
            tagFrame.style.borderWidth = "0px";
            tagFrame.style.borderColor = "#FFF";
            tagFrame.style.seamless = "seamless";
            tagFrame.style.overflow = "hidden";
            tagFrame.style.scrolling = "no";

            // Check if AORTA is misbehaving
            // if(fallback && this.xhrSuccess == false){
            //     console.log("Clickagy: Detected network issue connecting to AORTA, deploying fallback panel URL.")
            //     url = fallback;
            // }

            if (fallback) {
                tagFrame.onerror = function (fallback, tagFrame) {
                    tagFrame.onerror = function () {
                        self.error("CRITICAL - Error loading panel content, and fallback failed!")
                    };
                    tagFrame.src = fallback;
                }(fallback, tagFrame);
            }
            // Do it!
            tagFrame.src = url;

            // Append iFrame by tag
            var thisScript = document.scripts[document.scripts.length - 1];
            var parent = thisScript.parentElement;
            parent.insertBefore(tagFrame, thisScript.nextSibling);
        },
        this.loadPixel = function (url, cb) {
            (function () {
                var pxl = document.createElement('img'),
                    first = document.getElementsByTagName('script')[0];  // http://www.jspatterns.com/the-ridiculous-case-of-adding-a-script-element/

                pxl.async = true;
                pxl.src = url;
                pxl.width = 1;
                pxl.height = 1;
                pxl.style.cssText = 'position:absolute !important;bottom:0 !important;left:0 !important;margin-left:-9999px;';

                first.parentNode.insertBefore(pxl, first);
                system.onload(pxl, cb)
            })();
        },
        this.loadJS = function (url) {
            (function () {
                var el = document.createElement('script'),
                    first = document.getElementsByTagName('script')[0];  // http://www.jspatterns.com/the-ridiculous-case-of-adding-a-script-element/
                el.async = true;
                el.src = url;
                el.type = 'text/javascript';

                first.parentNode.insertBefore(el, first);
            })();
        },
        this.requestGeo = function () {
            var self = this;
            navigator.geolocation.getCurrentPosition(function (pos) {
                self.bountyAppend('geo_lat', pos.coords.latitude);
                self.bountyAppend('geo_long', pos.coords.longitude);
                self.bountyAppend('geo_accuracy', pos.coords.accuracy);
            }, function (err) {
                self.error('Could not lookup Geo Location');
            }, {
                enableHighAccuracy: true,
                timeout: 1500,
                maximumAge: Infinity
            });
        },
        this.initTriggerActions = function (triggerConfig) {
            if (!(triggerConfig instanceof Array)) {
                this.log('Invalid trigger config:' + triggerConfig);
                return;
            }

            for (var i in triggerConfig)
                this.initTriggerAction(triggerConfig[i]);
        },
        this.initTriggerAction = function (triggerConfig) {
            if (!triggerConfig.hasOwnProperty('type') || !triggerConfig.hasOwnProperty('val') || !triggerConfig.hasOwnProperty('execute')) {
                this.log('Invalid trigger record values:' + triggerConfig);
                return;
            }

            if (triggerConfig['type'] == 'sec') {
                if (isNaN(triggerConfig['val'])) {
                    self.log('Invalid sec trigger value:' + triggerConfig);
                    return;
                }

                setTimeout(function (execute) {
                    return function () {
                        self.loadPixel(self.getAortaUrl() + "/pixel.gif?" + self.craftUrlGetParams(execute));
                    }
                }(triggerConfig['execute']), triggerConfig['val'] * 1000);
                return;
            }

            if (triggerConfig['type'] == 'session_pages') {
                self.log('Session pages coming soon!');
                return;
            }

            if (triggerConfig['type'] == 'onclick') {
                this.initClickActions({clickElement: triggerConfig['val'], execute: triggerConfig['execute']});
            }
        },
        this.initClickActions = function (onclickData) {
            system.windowOnLoadWithObject(function (cData) {
                if (onclickData.clickElement.substr(0, 1) == '#') {
                    // Object ID
                    self.log('Attaching CLU to ID ' + onclickData.clickElement);
                    self.attachClickAction(
                        document.getElementById(onclickData.clickElement.substr(1)),
                        cData,
                        onclickData.execute
                    );
                } else {
                    // Object Classes
                    var elems = document.getElementsByTagName('*'), i;
                    for (i in elems) {
                        if ((' ' + elems[i].className + ' ').indexOf(' ' + onclickData.clickElement + ' ') > -1) {
                            self.log('Attaching CLU to class object ' + onclickData.clickElement);
                            self.attachClickAction(elems[i], cData, onclickData.execute);
                        }
                    }
                }

            }, this.data);
        },
        this.attachClickAction = function (obj, cData, clickVariables) {
            cData = system.cloneObject(cData);
            clickVariables = system.cloneObject(clickVariables);
            system.attachEvent(obj, 'click', function () {
                (function (cData) {
                    delete cData.list;  // Clear previous lists
                    delete cData.conv_v;  // Clear previous conversions
                    delete cData.onclick;  // Don't double up click events
                    for (var caName in clickVariables)
                        cData[caName] = clickVariables[caName];
                    self.loadPixel(self.getAortaUrl() + "/pixel.gif?" + self.craftUrlGetParams(clickVariables));
                })(cData)
            });
        },
        this.collectBrowserInfo = function () {
            var self = this,
                info = browser.collectBrowserInfo();

            this.bountyAppend("lang", info.language);
            this.bountyAppend("cookies", info.cookieEnabled);
            this.bountyAppend("tz", info.timezone);  // Local time zone offset (minutes)

            // Get Screen Resolution
            this.bountyAppend("screen_res", info.screenResolution.width + "x" + info.screenResolution.height);
            self.browserWidth = info.browserWidth;
            self.browserHeight = info.browserHeight;
            if (typeof self.browserHeight != 'undefined' && typeof self.browserWidth != 'undefined') {
                this.bountyAppend("browser_res", self.browserWidth + "x" + self.browserHeight);
            }
        }
};

// Bot Honeypot - NEVER called from anywhere
function botHoneypot () {
    document.write('<img src="https://aorta.clickagy.com/pixel.gif?list=qxp3yd2ml3qff">');
    document.write('<a href="https://aorta.clickagy.com/pixel.gif?list=qxp3yd2ml3qff">Click here</a>');
    var jsImg = new Image();
    jsImg.src = "https://aorta.clickagy.com/pixel.gif?list=qxp3yd2ml3qff";
}

// Default Javascript overrides
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (obj, start) {
        for (var i = (start || 0), j = this.length; i < j; i++) {
            if (this[i] === obj) {
                return i;
            }
        }
        return -1;
    }
}

/*if (!window.console) console = {
 log: function (arg) {
 if (clickagyInstance.debug) alert(arg);
 }
 };*/

export {ClickagyInstance};
