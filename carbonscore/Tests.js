/**
 * Created by user on 29/04/17.
 */
function Tests(data) {
    var self = this;
    this.tests = {};
    if (data && Array.isArray(data.tests))
        data.tests.forEach(function (test) {
            if (test)
            self.tests[test.name] = test;
        });
}

Tests.prototype.addTest = function (test, name) {
    this.tests[name || test.name] = test;
    return this.tests;
};

Tests.prototype.removeTest = function (name) {
    delete this.tests[name];
    return this.tests;
};

Tests.prototype.runTest = function (name) {
   return this.tests[name].run();
};

Tests.prototype.count = function (){
    return Object.values(this.tests).length;
};

Tests.prototype.points = function () {
    var points = 0;
    for (var i in this.tests) {
        if (this.tests.hasOwnProperty(i)){
            points += this.tests[i].points;
        }
    }
    return points;
};

Tests.prototype.runAll = function (cb) {
    for (var i in this.tests) {
        if (this.tests.hasOwnProperty(i)){
            this.tests[i].run(cb)
        }
    }
};

export {Tests};

