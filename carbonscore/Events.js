/**
 * Created by 4ant0m on 01.07.17.
 */
import {Event} from './Event.js';
import {system} from '../utils/system.js';

function Events(data) {
    var EVENTS = data && data.EVENTS || [],
        self = this;
    EVENTS.forEach(function (item) {
        self.push(new Event({name: item}));
    });
    return this;
}

system.extend(Events, Array);

Events.prototype.flush = function () {
    this.forEach(function (event) {
        event.clearCheck();
    })
};

Events.prototype.check = function (cb, isRepeat) {
    this.forEach(function (event) {
        event.check(cb, isRepeat)
    })
};

export {Events};