/**
 * Created by user on 29/04/17.
 */
import {system} from '../utils/system.js';
import {browser} from '../utils/browser.js';

function Event(data) {
    var self = this,
        context;
    this.name = data && data.name;
    this.checked = false;
    this.removeCheck = false;
    this.PASSIVE_EVENTS = ["wheel", "mousewheel", "touchstart", "touchmove"];
    this.supportsPassive = false;
    this.window = browser.window;
    this.document = browser.document;

    if (system.checkPassiveEventSupport() && this.PASSIVE_EVENTS.indexOf(this.name) != -1) {
        this.supportsPassive = true;
    }
    context = this.window;

    if (this.isSupported(data.name, this.window)) {
        context = this.window;
    }
    if (this.isSupported(data.name, this.document)) {
        context = this.document;
    }

    this.checkEvent = function (cb, isRepeat) {
        var e = function (event) {
            event = event || self.window.event;
            self.checked = true;
            if (!isRepeat || self.removeCheck) {
                self.removeEvent(context, self.name, e);
            }
            if (!self.removeCheck)
                cb(event);
        };
        self.addEvent(context, self.name, e);
    };
}

Event.prototype.isSupported = function (eventname, context) {
    return ('on' + eventname in context);
};

Event.prototype.addEvent = function (element, type, handler) {
    var self = this;
    if (element.addEventListener) {
        element.addEventListener(type, handler, self.supportsPassive ? {passive: true} : false);
    } else if (element.attachEvent) {
        element.attachEvent("on" + type, handler);
    }
};

Event.prototype.removeEvent = function (element, type, handler) {
    if (element.removeEventListener) {
        element.removeEventListener(type, handler, false);
    }
    else {
        var name = "on" + type;
        if (element.detachEvent) {
            if (typeof element[name] === 'undefined') {
                element[name] = null;
            }
            element.detachEvent(name, handler);
        }
    }
};

Event.prototype.check = function (cb, isRepeat) {
    cb = cb || function () {
        };
    return this.checkEvent(function (res) {
        cb(res)
    }, isRepeat);
};

Event.prototype.clearCheck = function () {
    this.removeCheck = true;
};

export {Event};