/**
 * Created by user on 25/04/17.
 */

function Test (data){
    this.id = data.id;
    this.name = data.name;
    this.src = data.src || function (){};
    this.points = data.points || 1;
    this.result = data.result;
}

Test.prototype.run = function (cb){
    var self = this;
  return this.src(function (res) {
      cb(res, self)
  });
};

export {Test};


