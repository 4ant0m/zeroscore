/**
 * Created by user on 29/04/17.
 */
import {InitTests} from '../tests/InitTests.js';
import {system} from '../utils/system.js';
import {DuplicateControl} from '../tag/DuplicateControl.js'

function Detection(data) {
    var self = this;

    this.duplicateControl = new DuplicateControl({
        cookie_key: data && data.cookie_key || 'cs',
        expiration_day: data.expiration_day,
        debug: data.debug
    });
    this.tests = new InitTests();
    this.interval_ms = data && data.interval_ms || 2000;
    this._updated = true;
    this.debug = data && data.debug || false;
    this.results = [];
    this.log = function () {
        var log = system.log("Clickagy [Detection]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };

    if (this.debug === true)
        this.log('Debug mode was enabled');

    if (!Date.now) {
        Date.now = function () {
            return new Date().getTime();
        }
    }
}

Detection.prototype.makeResult = function (test) {
    this.results.push({
        id: test.id,
        name: test.name,
        result: test.result
    });
    this._updated = false;
};


Detection.prototype.getResLink = function () {
    var link = '?cs=';
    for (var i = 0, length = this.results.length; i < length; i++) {
        link += this.results[i].id + ':' + this.results[i].result;
        if (i < length - 1)
            link += ','
    }
    return link;
};

Detection.prototype.getIntervalRes = function (cb) {
    var self = this;
    setInterval(function () {
        if (!self._updated) {
            self.results = self.duplicateControl.filterUnCompletedTests(self.results);
            if (self.results.length > 0) {
                cb(self.getResLink(), function () {
                    self.log('Link', self.getResLink());
                    self.duplicateControl.writeCompletedTests()
                });
            }
            self._updated = true;
            self.results = [];
        }
    }, this.interval_ms);
};

Detection.prototype.runTests = function () {
    var self = this;
    this.tests.runAll(function (res, test) {
        self.makeResult(test);
        self.log('Detection', test.name, test.id, res, test.result);
    })
};

export {Detection};