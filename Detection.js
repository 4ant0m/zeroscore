/**
 * Created by user on 29/04/17.
 */
import {tests} from './tests/index.js';
import {system} from './utils/system.js';

function Detection(data) {
    var self = this;
    this.tests = data && data.tests || tests;
    this.cookie_key = data && data.cookie_key || 'cs';
    this.completed_tests = this.parseCookie(this.cookie_key) || {};
    this.interval_ms = data && data.interval_ms || 2000;
    this.expiration_day = 1000 * 3600 * 24 * data.expiration_day || 30;
    this._updated = true;
    this.debug = data && data.debug || false;
    this.results = [];
    this.log = function () {
        var log = system.log("Clickagy [Detection]: ");
        if (self.debug) {
            log.apply(self, arguments);
        }
    };

    if (this.debug === true)
        this.log('Debug mode was enabled');

    if (!Date.now) {
        Date.now = function () {
            return new Date().getTime();
        }
    }
}

Detection.prototype.makeResult = function (test) {
    this.results.push({
        id: test.id,
        name: test.name,
        result: test.result
    });
    this._updated = false;
};

Detection.prototype.checkCookies = function () {
    var self = this,
        completed_tests = this.completed_tests;

    //read cookie
    this.results = this.results.filter(function (result) {
        if (completed_tests[result.id] == null || completed_tests[result.id] <= (Date.now + self.expiration_day)) {
            completed_tests[result.id] = Date.now();
            return true
        }
        return false;
    });
    self.log('Completed Tests: ', completed_tests);
};

Detection.prototype.writeCookie = function (key, results) {
    var cookie = JSON.stringify(results);
    system.setCookie(key, cookie);
    this.log('Cookies: ', document.cookie);
};

Detection.prototype.parseCookie = function (key) {
    var cookie = system.readCookie(key),
        self = this;
    if (cookie == null)
        return {};
    try {
        return JSON.parse(cookie);
    } catch (e) {
        self.log(e);
        return {}
    }
};

Detection.prototype.getResLink = function () {
    var link = '?cs=';
    for (var i = 0, length = this.results.length; i < length; i++) {
        link += this.results[i].id + ':' + this.results[i].result;
        if (i < length - 1)
            link += ','
    }
    return link;
};

Detection.prototype.getIntervalRes = function (cb) {
    var self = this;
    setInterval(function () {
        if (!self._updated) {
            self.checkCookies();
            if (self.results.length > 0)
                cb(self.getResLink(), function (){
                    self.writeCookie(self.cookie_key, self.completed_tests);
                });
            self._updated = true;
            self.results = [];
        }
    }, this.interval_ms);
};

Detection.prototype.runTests = function () {
    var self = this;
    this.tests.runAll(function (res, test) {
        self.makeResult(test);
        self.log('Detection', test.name, res, test.result);
        self.log('Link', self.getResLink());
    })
};

export {Detection};