/**
 * Created by 4ant0m on 10/24/17.
 */
import {system} from '../utils/system.js'; 
var browser = {
    window: system.canAccessToParent() ? window.top : window,
    document: system.canAccessToParent() ? window.top.document : document,
    getCookiePreference: function () {
        if (typeof network == 'undefined')
            return null;
        var cookiePreference = {
            cookieBehavior: network.cookie.cookieBehavior,
            lifetimePolicy: network.cookie.lifetimePolicy,
            lifetimeDays: network.cookie.lifetime.days,
            alwaysAcceptSessionCookies: network.cookie.alwaysAcceptSessionCookies,
            thirdpartySessionOnly: network.cookie.thirdparty.sessionOnly,
            maxNumber: network.cookie.maxNumber,
            maxPerHost: network.cookie.maxPerHost,
            disableCookieForMailNews: network.cookie.disableCookieForMailNews,
            prefsMigrated: network.cookie.prefsMigrated,
        };
        return cookiePreference;
    },
    getLocalStorage: function () {
        var test = 'localStorage_test';
        try {
            localStorage.setItem(test, test);
            localStorage.removeItem(test);
            return localStorage;
        } catch (e) {
            return null;
        }
    },
    getSessionStorage: function () {
        var test = 'sessionStorage_test';
        try {
            sessionStorage.setItem(test, test);
            sessionStorage.removeItem(test);
            return sessionStorage;
        } catch (e) {
            return null;
        }
    },
    getBattery: function (cb) {
        var battery = navigator.battery || navigator.webkitBattery || navigator.mozBattery || navigator.msBattery ||
            navigator.getBattery && navigator.getBattery();

        if (!battery) return cb(null);

        if (typeof battery.then == 'function') {
            battery.then(function (battery) {
                return cb(battery);

            }).catch(function (err) {
                return cb(null);
            })
        } else {
            return cb(battery);
        }
    },
    getIndexedDb: function () {
        var obj = {
            indexedDB: browser.window.indexedDB || browser.window.mozIndexedDB || browser.window.webkitIndexedDB || browser.window.msIndexedDB,
            IDBTransaction: browser.window.IDBTransaction || browser.window.webkitIDBTransaction || browser.window.msIDBTransaction,
            IDBKeyRange: browser.window.IDBKeyRange || browser.window.webkitIDBKeyRange || browser.window.msIDBKeyRange
        };
        return obj
    },
    getListOfPlugins: function () {
        var plugins = navigator && navigator.plugins,
            result = [];
        if (!plugins)
            return result;
        for (var i = 0; i < plugins.length; i++) {
            result.push(plugins[i].name);
        }
        return result;
    },
    getMimeTypes: function () {
        var mimeTypes = navigator && navigator.mimeTypes,
            result = [];
        if (!mimeTypes)
            return result;
        for (var i = 0; i < mimeTypes.length; i++) {
            result.push(mimeTypes[i].type);
        }
        return result;
    },
    getHeader: function (cb) {
        // Reasign the existing setRequestHeader function to
        // something else on the XMLHtttpRequest class
        XMLHttpRequest.prototype.wrappedSetRequestHeader =
            XMLHttpRequest.prototype.setRequestHeader;

        // Override the existing setRequestHeader function so that it stores the headers
        XMLHttpRequest.prototype.setRequestHeader = function (header, value) {
            // Call the wrappedSetRequestHeader function first
            // so we get exceptions if we are in an erronous state etc.
            this.wrappedSetRequestHeader(header, value);

            // Create a headers map if it does not exist
            if (!this.headers) {
                this.headers = {};
            }

            // Create a list for the header that if it does not exist
            if (!this.headers[header]) {
                this.headers[header] = [];
            }

            // Add the value to the header
            this.headers[header].push(value);
        };

        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (cb && typeof cb === 'function') {
                    var headers = request.getAllResponseHeaders().toLowerCase(),
                        aHeaders = headers.split('\n'),
                        data = {};
                    for (var i = 0; i < aHeaders.length; i++) {
                        var thisItem = aHeaders[i],
                            key = thisItem.substring(0, thisItem.indexOf(':'));
                        data[key] = thisItem.substring(thisItem.indexOf(':') + 1);
                    }
                    return cb(data);
                }
            }
        };
        request.open('HEAD', document.location, true);
        request.send(null);
    },
    isTouchDevice: function () {
        var el = document.createElement('div');
        el.setAttribute('ongesturestart', 'return;');
        if (typeof el.ongesturestart === "function") {
            return true;
        } else {
            el.setAttribute('ontouchstart', 'return;');
            return typeof el.ontouchstart === "function"
        }
    },
    checkMobile: function () {
        var userAgent = navigator && navigator.userAgent,
            detectMobileBrowsers = {
                fullPattern: /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i,
                shortPattern: /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i,
                tabletPattern: /android|ipad|playbook|silk/i
            };
        if (detectMobileBrowsers.fullPattern.test(userAgent)
            || detectMobileBrowsers.shortPattern.test(userAgent.substr(0, 4))
            || detectMobileBrowsers.tabletPattern.test(userAgent)) {
            return userAgent
        } else
            return false;
    },
    checkBrowserVersion: function () {
        var nVer = navigator && navigator.appVersion,
            nAgt = navigator && navigator.userAgent,
            browserName = navigator && navigator.appName,
            fullVersion = '' + parseFloat(navigator && navigator.appVersion),
            majorVersion = parseInt(navigator && navigator.appVersion, 10),
            nameOffset, verOffset, ix,
            is = {
                Opera: false,
                IE: false,
                Chrome: false,
                Safari: false,
                Firefox: false,
                Edge: false,
                Android: false,
                NaN: true
            };
        // In Android
        var is_native_android = ((nAgt.indexOf('Mozilla/5.0') > -1 && nAgt.indexOf('Android ') > -1 && nAgt.indexOf('AppleWebKit') > -1) && (nAgt.indexOf('Version') > -1));
        if (is_native_android && (verOffset = nAgt.indexOf("Android")) != -1) {
            browserName = "Android";
            fullVersion = nAgt.substring(verOffset + 8);
            is.Android = true;
            is.NaN = false;
        } else

        // In Opera 15+, the true version is after "OPR/"
        if ((verOffset = nAgt.indexOf("OPR/")) != -1) {
            browserName = "Opera";
            fullVersion = nAgt.substring(verOffset + 4);
            is.Opera = true;
            is.NaN = false;
        }
        // In older Opera, the true version is after "Opera" or after "Version"
        else if ((verOffset = nAgt.indexOf("Opera")) != -1) {
            browserName = "Opera";
            fullVersion = nAgt.substring(verOffset + 6);
            is.Opera = true;
            is.NaN = false;
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
        // In MSIE, the true version is after "MSIE" in userAgent
        else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
            browserName = "Microsoft Internet Explorer";
            fullVersion = nAgt.substring(verOffset + 5);
            is.IE = true;
            is.NaN = false;
        }
        // in MSIE - Trident
        else if ((verOffset = nAgt.indexOf("Trident")) != -1) {
            browserName = "Microsoft Internet Explorer";
            fullVersion = nAgt.substring(verOffset + 5);
            is.IE = true;
            is.NaN = false;
        }

        // In Chrome, the true version is after "Chrome"
        else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
            browserName = "Chrome";
            fullVersion = nAgt.substring(verOffset + 7);
            is.Chrome = true;
            is.NaN = false;
        }
        // In Safari, the true version is after "Safari" or after "Version"
        else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
            browserName = "Safari";
            fullVersion = nAgt.substring(verOffset + 7);
            is.Safari = true;
            is.NaN = false;
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
        // In Firefox, the true version is after "Firefox"
        else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
            browserName = "Firefox";
            fullVersion = nAgt.substring(verOffset + 8);
            is.Firefox = true;
            is.NaN = false;
        }
        // In Edge
        else if ((verOffset = nAgt.indexOf("Edge")) != -1) {
            browserName = "Edge";
            fullVersion = nAgt.substring(verOffset + 5);
            is.Edge = true;
            is.NaN = false;
        }
        // In most other browsers, "name/version" is at the end of userAgent
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
            (verOffset = nAgt.lastIndexOf('/'))) {
            browserName = nAgt.substring(nameOffset, verOffset);
            fullVersion = nAgt.substring(verOffset + 1);
            if (browserName.toLowerCase() == browserName.toUpperCase()) {
                browserName = navigator.appName;
            }
        }
        // trim the fullVersion string at semicolon/space if present
        if ((ix = fullVersion.indexOf(";")) != -1)
            fullVersion = fullVersion.substring(0, ix);
        if ((ix = fullVersion.indexOf(" ")) != -1)
            fullVersion = fullVersion.substring(0, ix);

        majorVersion = parseInt('' + fullVersion, 10);
        if (isNaN(majorVersion)) {
            fullVersion = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }
        return {
            nVer: nVer,
            nAgt: nAgt,
            browserName: browserName,
            fullVersion: fullVersion,
            majorVersion: majorVersion,
            is: is
        }
    },
    checkBrowser: function () {
        // Opera 8.0+
        var isOpera = (!!browser.window.opr && !!opr.addons) || !!browser.window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]"
        var isSafari = /constructor/i.test(browser.window.HTMLElement) || (function (p) {
                return p.toString() === "[object SafariRemoteNotification]";
            })(!window['safari'] || safari.pushNotification);

        // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        var isEdge = !isIE && !!browser.window.StyleMedia;

        // Chrome 1+
        var isChrome = !!browser.window.chrome && !!browser.window.chrome.webstore;

        // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!browser.window.CSS;

        var res = {
            userAgent: navigator.userAgent,
            isOpera: isOpera,
            isFirefox: isFirefox,
            isSafari: isSafari,
            isEdge: isEdge,
            isChrome: isChrome,
            isIE: isIE,
            isBlink: isBlink
        };

        return res;
    },
    collectBrowserInfo: function () {
        var info = {
                referrer: document && document.referrer,
                language: navigator && navigator.userLanguage || navigator.language || "",
                cookieEnabled: navigator && navigator.cookieEnabled && navigator.cookieEnabled !== "undefined",
                timezone: new Date().getTimezoneOffset(),
                screenResolution: {
                    width: screen && screen.width,
                    height: screen && screen.height
                },
                userAgent: navigator && navigator.userAgent,
                colorDepth: screen && screen.colorDepth,
                hardwareConcurrency: navigator && navigator.hardwareConcurrency,
                platform: navigator && navigator.platform,
                DNT: navigator && navigator.doNotTrack,
                characterSet: document && document.characterSet
            },
            doc = browser.document,
            win = browser.window;

        var offsetWidth = doc && doc.body && doc.body.offsetWidth,
            clientWidth = doc && doc.body && doc.documentElement && doc.documentElement.clientWidth,
            innerWidth = win && win.innerWidth,
            offsetHeight = doc && doc.body && doc.body.offsetHeight,
            clientHeight = doc && doc.body && doc.documentElement && doc.documentElement.clientHeight,
            innerHeight = win && win.innerHeight;

        info.browserWidth = Math.max(offsetWidth, clientWidth, innerWidth);
        info.browserHeight = Math.max(offsetHeight, clientHeight, innerHeight);

        return info;
    },
    checkScreenSize: function (width, height) {
        var body = browser.getBodySize();
        if (body.width >= width && body.height >= height) {
            return true;
        }
        else return false;
    },
    getBodySize: function () {
        var w = Math.max(
            document.documentElement["clientWidth"],
            document.body["scrollWidth"],
            document.documentElement["scrollWidth"],
            document.body["offsetWidth"],
            document.documentElement["offsetWidth"]
        );
        var h = Math.max(
            document.documentElement["clientHeight"],
            document.body["scrollHeight"],
            document.documentElement["scrollHeight"],
            document.body["offsetHeight"],
            document.documentElement["offsetHeight"]
        );
        return {
            width: w,
            height: h
        }
    },
    getFileAPI: function () {
        var FileReader = browser.window.File || browser.window.FileReader || browser.window.FileList || browser.window.Blob;
        if (FileReader) {
            return FileReader;
        }
        else
            return false;
    },
    checkBrowserSupportByPattern: function (PATTERN) {
        var browserVersion = browser.checkBrowserVersion(),
            isMobile = browser.checkMobile(),
            version = browserVersion.fullVersion;
        version = version.substring(0, version.indexOf('.', version.indexOf('.') + 1));

        var pattern = isMobile ? PATTERN.mobile : PATTERN.pc;

        for (var current_browser in pattern) {
            if (pattern.hasOwnProperty(current_browser))
                if (browserVersion.is[current_browser]) {
                    if (version >= pattern[current_browser]) {
                        return true;

                    }
                }
        }
        return false;
    },
    getMobileOperatingSystem: function () {
        var userAgent = navigator.userAgent || navigator.vendor || browser.window.opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
            return "Windows Phone";
        }

        if (/android/i.test(userAgent)) {
            return "Android";
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !browser.window.MSStream) {
            return "iOS";
        }

        return "unknown";
    }

};

export {browser};